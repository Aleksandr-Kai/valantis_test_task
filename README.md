# Тестовое задание для Валантис

Используя предоставленный апи создать страницу, которая отображает список товаров.
Для каждого товара должен отображаться его id, название, цена и бренд.

[Полный текст задания](https://github.com/ValantisJewelry/TestTaskValantis)

[API Doc](https://github.com/ValantisJewelry/TestTaskValantis/blob/main/API.md)
