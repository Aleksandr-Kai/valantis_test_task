import { getProducts } from "./components/api";
import { createBuffer } from "./components/prodbuffer";
import { filterItems } from "./components/utils";
import "./styles/index.css";

const itemTemplate = document.querySelector("#product-item-template").content;
const productListElement = document.querySelector(".product-list");
const buttonPrev = document.querySelector(".button-prev");
const buttonNext = document.querySelector(".button-next");
const pageNumber = document.querySelector(".page-number");
const buttonAcceptFilter = document.querySelector(".button-accept-filter");
const buttonFilter = document.querySelector(".button-filter-show");
const panelFilter = document.querySelector(".filter");
const inputBrand = document.querySelector("#filter_brand");
const inputName = document.querySelector("#filter_name");
const inputPrice = document.querySelector("#filter_price");

const pageSize = 6;

const getProductsBuff = createBuffer(getProducts);

function renderProductList(items) {
	const filtred = filterItems(items);
	const itemElements = filtred.map((item) => {
		const li = itemTemplate.cloneNode(true);
		li.querySelector(".product-item__name").textContent = item.product;
		li.querySelector(".product-item__id").textContent = item.id;
		li.querySelector(".pruduct-item__brand").textContent =
			item.brand || "unknown brand";
		li.querySelector(".product-item__price").textContent = item.price;
		return li;
	});
	productListElement.textContent = "";
	itemElements.forEach((element) => productListElement.appendChild(element));
}

function getFilter() {
	const filter = {};
	if (inputBrand.value !== "") filter.brand = inputBrand.value;
	if (inputName.value !== "") filter.product = inputName.value;
	if (inputPrice.value !== "") filter.price = Number(inputPrice.value);
	return Object.keys(filter).length ? filter : undefined;
}

function loadProductPage(page) {
	productListElement.scrollTop = 0;
	productListElement.textContent = "Request products...";

	if (page === 0) buttonPrev.classList.add("disabled");
	else buttonPrev.classList.remove("disabled");

	pageNumber.textContent = page + 1;
	getProductsBuff(
		page,
		(prods) => {
			if (prods.length > 0) {
				renderProductList(prods);
			} else {
				productListElement.textContent = "Products can`t be loaded.";
			}
		},
		{
			bufferStatus: (prev, next) => {
				next
					? buttonNext.classList.remove("disabled")
					: buttonNext.classList.add("disabled");
				prev
					? buttonPrev.classList.remove("disabled")
					: buttonPrev.classList.add("disabled");
			},
			filter: getFilter(),
		}
	);
}

function getCurrentPage() {
	return Number(pageNumber.textContent);
}

buttonPrev.addEventListener("click", () => {
	let prevPage = getCurrentPage() - 2;
	prevPage = prevPage < 0 ? 0 : prevPage;
	loadProductPage(prevPage);
});

buttonNext.addEventListener("click", () => {
	const nextPage = getCurrentPage();
	loadProductPage(nextPage);
});

buttonFilter.addEventListener("click", () => {
	panelFilter.classList.remove("collapsed");
});

buttonAcceptFilter.addEventListener("click", () => {
	panelFilter.classList.add("collapsed");
	loadProductPage(-10);
	pageNumber.textContent = 1;
});

loadProductPage(0);
