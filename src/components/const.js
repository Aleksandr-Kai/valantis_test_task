import MD5 from "crypto-js/md5";

const date = new Date();

const auth_str = `Valantis_${date.getUTCFullYear()}${(date.getUTCMonth() + 1)
	.toString()
	.padStart(2, "0")}${date.getUTCDate().toString().padStart(2, "0")}`;

export const api_config = {
	url: "http://api.valantis.store:40000/",
	headers: {
		"Content-Type": "application/json",
		"X-Auth": MD5(auth_str).toString(),
	},
};

export const prodPageSize = 50;
