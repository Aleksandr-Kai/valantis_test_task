export function filterItems(items) {
	const filtred = [];
	items.forEach((item) => {
		if (filtred.find(({ id }) => item.id === id)) return;
		filtred.push(item);
	});
	return filtred;
}

export function logError(error) {
	console.log(error);
}
