import { api_config, prodPageSize } from "./const";

const requestRepeatTimeout = 1000;
const requestFailsLimit = 3;

let controller = new AbortController();

function checkResponse(resp) {
	if (resp.ok) return resp.json().then((data) => data.result);
	return Promise.reject(`Status: ${resp.status}`);
}

export async function apiRequest(params) {
	return fetch(api_config.url, {
		method: "POST",
		headers: api_config.headers,
		body: JSON.stringify(params),
		signal: controller.signal,
	}).then(checkResponse);
}

function isAbortError(err) {
	return err.toString().indexOf("AbortError") >= 0;
}

function fillIDsPage(offset, callback, filter) {
	let res = [];
	let failCounter = requestFailsLimit;
	const body = filter
		? {
				action: "filter",
				params: filter,
		  }
		: {
				action: "get_ids",
				params: { offset: offset, limit: prodPageSize },
		  };
	function tryRequest(offset) {
		apiRequest(body)
			.then((ids) => {
				const idsSet = [...new Set(ids)];
				res.push(...idsSet.slice(0, prodPageSize - res.length));
				if (prodPageSize - res.length) tryRequest(offset + prodPageSize);
				else callback(res);
			})
			.catch((err) => {
				if (isAbortError(err)) return;
				console.log(`(${requestFailsLimit - --failCounter}) ${err}`);
				if (failCounter)
					setTimeout(() => tryRequest(offset), requestRepeatTimeout);
				else callback([]);
			});
	}
	tryRequest(offset);
}

export function getProducts(page, callback, filter) {
	if (page < 0) {
		callback([]);
		return;
	}
	let failCounter = requestFailsLimit;

	function tryRequest() {
		fillIDsPage(
			prodPageSize + page,
			(ids) => {
				if (ids.length === 0) {
					callback([]);
					return;
				}
				apiRequest({
					action: "get_items",
					params: { ids },
				})
					.then(callback)
					.catch((err) => {
						if (isAbortError(err)) {
							console.log("Aborted");
							return;
						}
						console.log(`(${requestFailsLimit - --failCounter}) ${err}`);
						if (failCounter)
							setTimeout(() => tryRequest(), requestRepeatTimeout);
						else callback([]);
					});
			},
			filter
		);
	}
	tryRequest();
}

export function abortAllRequests() {
	console.log("Abort all");
	controller.abort();
	controller = new AbortController();
}
