import { abortAllRequests } from "./api";

export function createBuffer(prodGetter) {
	let next = [];
	let prev = [];
	let curr = [];
	let currentPage = -10;

	function loadCurrent(callback, filter) {
		curr = [];
		prodGetter(
			currentPage,
			(prods) => {
				curr = [...prods];
				callback(curr);
			},
			filter
		);
	}

	function preloadNext(report) {
		next = [];
		report && report();
		prodGetter(currentPage + 1, (prods) => {
			next = [...prods];
			report && report();
		});
	}

	function preloadPrev(report) {
		prev = [];
		report && report();
		prodGetter(currentPage - 1, (prods) => {
			prev = [...prods];
			report && report();
		});
	}

	function nextBufferized() {
		return next.length > 0;
	}
	function prevBufferized() {
		return prev.length > 0;
	}
	function currBufferized() {
		return curr.length > 0;
	}

	return (page, callback, options) => {
		if (options.filter) page = -10;
		switch (currentPage - page) {
			case -1: // next
				abortAllRequests();
				currentPage++;
				prev = [...curr];
				curr = [...next];

				preloadNext(() =>
					options.bufferStatus(prevBufferized(), nextBufferized())
				);

				if (currBufferized()) {
					callback(curr);
				} else {
					loadCurrent(callback);
				}

				if (!prevBufferized())
					preloadPrev(() =>
						options.bufferStatus(prevBufferized(), nextBufferized())
					);
				break;

			case 1: // prev
				abortAllRequests();
				currentPage--;
				next = [...curr];
				curr = [...prev];

				preloadPrev(() =>
					options.bufferStatus(prevBufferized(), nextBufferized())
				);

				if (currBufferized()) {
					callback(curr);
				} else {
					loadCurrent(callback);
				}

				if (!nextBufferized())
					preloadNext(() =>
						options.bufferStatus(prevBufferized(), nextBufferized())
					);
				break;

			case 0: // current
				if (currBufferized()) {
					callback(curr);
				} else {
					loadCurrent(callback, options.filter);
				}
				if (!options.filter) {
					if (!nextBufferized())
						preloadNext(() =>
							options.bufferStatus(prevBufferized(), nextBufferized())
						);
					if (!prevBufferized())
						preloadPrev(() =>
							options.bufferStatus(prevBufferized(), nextBufferized())
						);
				}
				break;

			default: // reset
				prev = [];
				next = [];
				options.bufferStatus(prevBufferized(), nextBufferized());
				currentPage = 0;
				loadCurrent(callback, options.filter);
				if (!options.filter) {
					preloadNext(() =>
						options.bufferStatus(prevBufferized(), nextBufferized())
					);
					preloadPrev(() =>
						options.bufferStatus(prevBufferized(), nextBufferized())
					);
				}
		}
	};
}
